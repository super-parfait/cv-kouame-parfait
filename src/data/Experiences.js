const DataExperiences = [
    {
        id: 1,
        titre: "Developpeur Web et Mobile Junior",
        date: "Fevrier 2022-Fevrier 2023",
        lieu: "Orange Digital Center-Plateau",
        texte: "Conception d'une plateforme web et une application mobile couplé à un systeme de surveillance des constantes de santé d'une personne ayant enfilé un sous vêtement connecté que nous avons conçu. Aussi vainqueur du premier prix Orange Summer Challenge"
    },
    {
        id: 2,
        titre: "Developpeur Web / Webmaster",
        date: "Mars 2021-Mai 2021",
        lieu: "GIMAMFI",
        texte: "Concepteur-Webmaster du site : (https://www.gimamfi.com) , et cela pour une Entreprise Ivoiro francaise dans le but de faire connaître ses activités."
    },
    {
        id: 3,
        titre: " Conception de site web avec le framework laravel",
        date: "Juillet 2020- Septembre 2020",
        lieu: "Freelance",
        texte: "Une période pendant laquelle j’ai conçu un site web ecommerce pour des ventes en lignes avec le framework LARAVEL qui seront mis en ligne."
    },
    {
        id: 4,
        titre: "Developpeur Web",
        date: "Juillet 2019 – Decembre 2019",
        lieu: "SODEC",
        texte: "Conception d'une plateforme web concernant l’identification des étudiants de l’Ecole Nationale Supérieure (ENS) de Côte d’Ivoire dans le but d’améliorer la gestion des étudiants(Phase de Test). site web:(https://kouame.alwaysdata.net/users)."
    },
    {
        id: 5,
        titre: "Développeur Web",
        date: "Aout 2022- Fevrier 2023",
        lieu: "SE-CONNAPE",
        texte: "Refonte de la plateforme web, Ainsi developpées la platefome d'administration et celle des utilisateurs. site web:(https://kouame.alwaysdata.net/users)."

    },
]

export default DataExperiences; 