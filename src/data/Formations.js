const DataFormations = [
    {
        id: 1,
        nom: "Baccalaureat Serie D",
        date: "2015-2016",
        lieu: "Lycée Moderne d'Abobo"
    },
    {
        id: 2,
        nom: "Licence Developpement d'Applications et e-Service",
        date: "2017-2019",
        lieu: "Université Virtuelle de Cote d'Ivoire - 2Plateaux"
    },
    {
        id: 3,
        nom: "Formation Developpeur Javascript-NodeJs",
        date: "2020-2021",
        lieu: "NaN- Cocody Angre Gestoci"
    },
    {
        id: 4,
        nom: "Master 1 Big Data Analytics",
        date: "2022-2023",
        lieu: "Université Virtuelle de Cote d'Ivoire - 2Plateaux"
    },
    {
        id: 5,
        nom: "Stage-Formation",
        date: "2022-2023",
        lieu: "Orange Digital Center-Plateau"
    },
]

export default DataFormations; 