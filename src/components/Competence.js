import React from 'react';

const Competence = ({titre,note}) => {
    return (
        <div className='competence'>
            <p className="titre_competence">{titre} </p>
            <div className="note_competence">
                <div className="note_competence">
                    <div className={`circle ${note > 0 && "circle-plain"}`}></div>
                    <div className={`circle ${note > 1 && "circle-plain"}`}></div>
                    <div className={`circle ${note > 2 && "circle-plain"}`}></div>
                    <div className={`circle ${note > 3 && "circle-plain"}`}></div>
                    <div className={`circle ${note > 4 && "circle-plain"}`}></div>
                </div>
            </div>
        </div>
    );
}

export default Competence;
