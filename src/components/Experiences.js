import React from 'react';

const Experiences = ({datas}) => {
    return (
        <div className="cursus mb3">
            <h2 className="h2">Experiences Professionnelles</h2>
            {datas.map(item =>(
                <div className="grid_row" key={item.id}>
                    <div className="grid_item">
                        <p className="grid_date">{item.date}</p>
                    </div>
                    <div className="grid_item">
                        <h3 className="grid_title">{item.titre}</h3>
                        <p className="grid_location">{item.lieu}</p>
                        <p className="grid_text">{item.texte}</p>
                    </div>
                </div>
            ))}
            
        </div>
    );
}

export default Experiences;
