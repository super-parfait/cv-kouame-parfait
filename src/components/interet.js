import React from 'react';

const Interet = () => {
    return (
        <div className='skills'>
            <h2 className="h2">Centres d'Intérêt</h2>
            <ul>
                <li>Informatique</li>
                <li>Lecture</li>
                <li>Sport</li>
            </ul>
        </div>
    );
}

export default Interet;
