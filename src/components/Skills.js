import React from 'react';
import Skill from "./Competence";
import Interet from './interet';
import "./Skills.css"


const Skills = () => {
    return (
        <>
            <div className="skills">
                <h2 className="h2">Compétences</h2>
                <Skill titre="HTML"  note="5"/>
                <Skill titre="CSS"  note="5"/>
                <Skill titre="REACT JS"  note="4"/>
                <Skill titre="NODE JS"  note="3"/>
                <Skill titre="ELECTRON JS"  note="3"/>
                <Skill titre="PHP LARAVEL"  note="4"/>
                <Skill titre="FLUTTER"  note="3"/>
                <Skill titre="BASE DE DONNEES"  note="3"/>
                <Skill titre="ELECTRONIQUE IOT"  note="3"/>
            </div>
            <div className="skills">
                <h2 className="h2">Langues</h2>
                <Skill titre="FRANCAIS"  note="5"/>
                <Skill titre="ANGLAIS"  note="3"/>
            </div>
            <div className="skills">
                <Interet/>
            </div>
        </>
    );
}

export default Skills;
