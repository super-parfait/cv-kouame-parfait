import React from 'react';
import "./User.css"
import HomeIcon from '@mui/icons-material/Home';
import PhoneIcon from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';
import EventIcon from '@mui/icons-material/Event';
import LocationOnIcon from '@mui/icons-material/LocationOn';

const User = () => {
    return (
        <div className='user'>
             <img src="./images/parfait_kouame.jpg" alt=""  className='user_avatar'/>
             <h1 className="user_name">Kouame Djiri Guy Parfait</h1>
             <p className="user_profesion">Développeur Web et Mobile</p>
             <div className="user_infos">
                <p className="user_info"> <HomeIcon /> Abidjan-2Plateaux Dokui</p>
                <p className="user_info">
                    <PhoneIcon /> <a href="tel:+225 07 58 58 34 98">07 58 58 34 98</a> / <a href="tel:+225 01 53 96 96 47">01 53 96 96 47</a></p>
                <p className="user_info"> <EmailIcon /> <a href="mailto:djiri.kouame@uvci.edu.ci">djiri.kouame@uvci.edu.ci</a></p>
                <p className="user_info"> <LocationOnIcon />Lieu de Naissance : Abobo</p>
                <p className="user_info"> <EventIcon /> Date de Naissance : 06/01/1998</p>
             </div>
             
        </div>
    );
}

export default User;
