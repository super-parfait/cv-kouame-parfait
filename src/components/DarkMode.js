import React from 'react';
import {useState} from 'react'
import WbSunnyOutlinedIcon from '@mui/icons-material/WbSunnyOutlined';
import Brightness2OutlinedIcon from '@mui/icons-material/Brightness2Outlined';

const DarkMode = () => {


    const [themeMode, setThemeMode] = useState("light");
    let iscliked = "clicked"
    const body = document.body
    const lightTheme = "light"
    const darkTheme = "dark"
    let theme

    // Je verifie le local storage pour connaitre le fond enregistré par mon navigateur
    if (localStorage) {
        theme = localStorage.getItem("theme")
    }

    if (theme === lightTheme || theme === darkTheme) {
        body.classList.add(theme)
    }else{
        body.classList.add(lightTheme)
    }


    // ma fonction pour changer de mode au click du bouton
    const changement = e =>{
        if (theme===darkTheme) {
            body.classList.replace(darkTheme, lightTheme)
            e.target.classList.remove(iscliked)
            localStorage.setItem('theme', 'light')
            theme = lightTheme
        }else{
            body.classList.replace(lightTheme, darkTheme)
            e.target.classList.add(iscliked)
            localStorage.setItem('theme', 'dark')
            theme = darkTheme
        }
        setThemeMode(theme)
    }
    return (
        <button className={theme==='dark' ? iscliked : ""} id="darkMode" onClick={e=>changement(e)} >
            {themeMode==="light" || theme==="light" ? <Brightness2OutlinedIcon/> : <WbSunnyOutlinedIcon/>}
        </button>
    );
}

export default DarkMode;
