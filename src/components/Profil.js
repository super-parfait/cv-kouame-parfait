import React from 'react';

const Profil = () => {
    return (
        <div className='profil mb5'>
            <h2 className="h2">Profil</h2>
            <p>
            Très passionné par les TIC, je serai honoré de faire partie de votre dynamique équipe pour relever plus de défis.
            </p>
        </div>
    );
}

export default Profil;
