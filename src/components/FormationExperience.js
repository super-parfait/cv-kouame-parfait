import React from 'react';
import DataFormations from '../data/Formations';
import Formations from './Formations';
import './FormationExperience.css'
import DataExperiences from '../data/Experiences';
import Experiences from './Experiences';

const FormationExperience = () => {
    return (
        <>
            <Formations datas={DataFormations} />
            <Experiences datas={DataExperiences} />
        </>
    );
}

export default FormationExperience;
