import './App.css';
import User from './components/User'
import Skills from './components/Skills'
import Profil from './components/Profil';
import FormationExperience from './components/FormationExperience';
import DarkMode from './components/DarkMode';
import { Preview, print } from 'react-html2pdf';
import  PictureAsPdfIcon  from '@mui/icons-material/PictureAsPdf';


function App() {
  function isMobileDevice() { 
    if( navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
    ){
       console.log( "c'est un telephone mobile");
     }
    else {
       console.log ("Non c'est un pc");
     }
   }
   isMobileDevice()
  const handleGenerationPdf = ()=>{
    let cvTemplate =  document.getElementById('cv-print')
    cvTemplate.setAttribute('style', 'width:210mm !important')
    cvTemplate.classList.add("cv-print")
    document.body.classList.remove("dark")
    
    setTimeout(()=>{
      print('CV Kouame Djiri Guy Parfait', 'cv-print')
      cvTemplate.setAttribute("style", "width:100% !important")
      cvTemplate.classList.remove('cv-print')
    }, 3000)
  }


  return (
    <Preview id={'cv-print'} >
      <div className="App">
        <div className='grid_container'>
          <div className='sidebar'>
            <div className="actions">
              <DarkMode/>
              <button onClick={handleGenerationPdf}> <PictureAsPdfIcon/> </button>
            </div>
          
            <User/>
            <Skills />
          </div>
          <div className='main'>
              <Profil/>
              <FormationExperience/>
          </div>
        </div>
      </div>
    </Preview>
  );
}

export default App;
